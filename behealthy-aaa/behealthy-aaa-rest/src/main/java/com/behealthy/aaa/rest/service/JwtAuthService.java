package com.behealthy.aaa.rest.service;

import com.behealthy.aaa.api.entity.Permission;
import com.behealthy.aaa.api.entity.User;
import com.behealthy.aaa.api.service.AuthService;
import com.behealthy.aaa.security.jwt.JwtTokenUtil;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class JwtAuthService implements AuthService {

    private final JwtTokenUtil jwtTokenUtil;
    private final JdbcUserService jdbcUserService;
    private final PasswordEncoder passwordEncoder;

    public JwtAuthService(JwtTokenUtil jwtTokenUtil, JdbcUserService jdbcUserService, PasswordEncoder passwordEncoder) {
        this.jwtTokenUtil = jwtTokenUtil;
        this.jdbcUserService = jdbcUserService;
        this.passwordEncoder = passwordEncoder;
    }

    /**
     * @param user which auth
     * @return jwt token if user successfully pass auth else return null
     */
    @Override
    public String auth(User user) {
        User registeredUser = jdbcUserService.getUserByName(user.getName());
        if (isCorrectUserCredentials(user, registeredUser)) {
            List<Permission> permissions = jdbcUserService.getUserPermissions(registeredUser);
            return jwtTokenUtil.generate(registeredUser, permissions);
        }
        return null;
    }

    private boolean isCorrectUserCredentials(User user, User registeredUser) {
        return user.getName().equals(registeredUser.getName())
                && passwordEncoder.matches(user.getPassword(), registeredUser.getPassword());
    }
}