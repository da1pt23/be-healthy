package com.behealthy.aaa.rest;

import com.behealthy.aaa.api.service.TokenService;
import com.behealthy.aaa.rest.repository.TokenRepository;
import com.behealthy.aaa.rest.service.JdbcTokenService;
import com.behealthy.aaa.security.interceptor.JwtInterceptor;
import com.behealthy.aaa.security.jwt.JwtTokenUtil;
import com.behealthy.aaa.security.jwt.PemKeyReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;

@Configuration
public class SecurityConfiguration implements WebMvcConfigurer {

    @Autowired
    private JwtInterceptor jwtInterceptor;

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public JwtTokenUtil jwtTokenUtil(@Value("${jwt.path.key.public}") String publicKeyFilePath,
                                     @Value("${jwt.path.key.private}") String privateKeyFilePath,
                                     @Value("${jwt.algorithm.type}") String algorithmType,
                                     @Value("${jwt.algorithm.name}") String algorithmName,
                                     @Value("${jwt.issuer}") String issuer,
                                     @Value("${jwt.token.expiration.timeout}") Long tokenExpirationTimeout) throws Exception {
        RSAPrivateKey privateKey = PemKeyReader.readPrivateKeyFromFile(privateKeyFilePath, algorithmType);
        RSAPublicKey publicKey = PemKeyReader.readPublicKeyFromFile(publicKeyFilePath, algorithmType);
        return new JwtTokenUtil(privateKey, publicKey, algorithmName, issuer, tokenExpirationTimeout);
    }

    @Bean
    public TokenService tokenService(TokenRepository tokenRepository) {
        return new JdbcTokenService(tokenRepository);
    }

    @Bean
    public JwtInterceptor jwtInterceptor(JwtTokenUtil jwtTokenUtil, TokenService tokenService) {
        return new JwtInterceptor(jwtTokenUtil, tokenService);
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(jwtInterceptor)
                .addPathPatterns("/**");
    }
}
