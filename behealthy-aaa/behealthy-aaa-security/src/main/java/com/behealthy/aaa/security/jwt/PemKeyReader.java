//Copyright 2017 - https://github.com/lbalmaceda
//Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

package com.behealthy.aaa.security.jwt;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;


public class PemKeyReader {

    private final static Logger logger = LoggerFactory.getLogger(PemKeyReader.class);

    private final static String PRIVATE_KEY_PEM_FILE_HEADER = "-----BEGIN PRIVATE KEY-----";
    private final static String PRIVATE_KEY_PEM_FILE_FOOTER = "-----END PRIVATE KEY-----";
    private final static String PUBLIC_KEY_PEM_FILE_HEADER = "-----BEGIN PUBLIC KEY-----";
    private final static String PUBLIC_KEY_PEM_FILE_FOOTER = "-----END PUBLIC KEY-----";
    private final static String LINE_SPLITTER = System.lineSeparator();

    /**
     * Read private key from file
     * If could not read file or wrong rsa algorithm throws exception
     *
     * @param filepath      private key file path
     * @param algorithmType name of algorithm type
     *                      (e.g <p>'RSA', 'DSA'</p>)
     * @throws NoSuchAlgorithmException if pass incorrect algorithm type
     * @throws InvalidKeySpecException  if not valid private key
     * @throws IOException              on error
     */
    public static RSAPrivateKey readPrivateKeyFromFile(String filepath, String algorithmType) throws NoSuchAlgorithmException, IOException, InvalidKeySpecException {
        try {
            String privateKeyContent = new String(Files.readAllBytes(Paths.get(filepath)));
            privateKeyContent = privateKeyContent.replaceAll(LINE_SPLITTER, "").replace(PRIVATE_KEY_PEM_FILE_HEADER, "").replace(PRIVATE_KEY_PEM_FILE_FOOTER, "");
            KeyFactory kf = KeyFactory.getInstance(algorithmType);
            PKCS8EncodedKeySpec keySpecPKCS8 = new PKCS8EncodedKeySpec(Base64.getDecoder().decode(privateKeyContent));
            return (RSAPrivateKey) kf.generatePrivate(keySpecPKCS8);
        } catch (Exception e) {
            logger.error("Could not read {} private key from file: {}", algorithmType, filepath, e);
            throw e;
        }
    }


    /**
     * Read public key from file
     * If could not read file or wrong rsa algorithm throws exception
     *
     * @param filepath public key file path
     * @param algorithmType name of algorithm type
     *                     (e.g <p>'RSA', 'DSA'</p>)
     * @throws NoSuchAlgorithmException if pass incorrect algorithm type
     * @throws InvalidKeySpecException if not valid private key
     * @throws IOException on error
     */
    public static RSAPublicKey readPublicKeyFromFile(String filepath, String algorithmType) throws NoSuchAlgorithmException, IOException, InvalidKeySpecException {
        try {
            String publicKeyContent = new String(Files.readAllBytes(Paths.get(filepath)));
            publicKeyContent = publicKeyContent.replaceAll(LINE_SPLITTER, "").replace(PUBLIC_KEY_PEM_FILE_HEADER, "").replace(PUBLIC_KEY_PEM_FILE_FOOTER, "");
            KeyFactory kf = KeyFactory.getInstance(algorithmType);
            X509EncodedKeySpec keySpecX509 = new X509EncodedKeySpec(Base64.getDecoder().decode(publicKeyContent));
            return (RSAPublicKey) kf.generatePublic(keySpecX509);
        } catch (Exception e) {
            logger.error("Could not read {} public key from file: {}", algorithmType, filepath, e);
            throw e;
        }
    }
}