package com.behealthy.profile.service;

import com.behealthy.profile.entity.Address;
import com.behealthy.profile.repository.AddressRepository;
import org.springframework.stereotype.Service;
import org.springframework.util.IdGenerator;

import java.util.UUID;

@Service
public class AddressService {

    private final IdGenerator idGenerator;
    private final AddressRepository addressRepository;

    public AddressService(IdGenerator idGenerator, AddressRepository addressRepository) {
        this.idGenerator = idGenerator;
        this.addressRepository = addressRepository;
    }

    public Address createProfileAddress(Address address){
        address.setId(idGenerator.generateId());
        addressRepository.createAddress(address);
        return address;
    }

    public void updateAddress(Address address) {
         addressRepository.updateAddress(address);
    }

    public Address getAddressById(UUID id) {
        return addressRepository.getAddressById(id);
    }
}
