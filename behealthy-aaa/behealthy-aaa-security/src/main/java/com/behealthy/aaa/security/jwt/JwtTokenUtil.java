package com.behealthy.aaa.security.jwt;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.behealthy.aaa.api.entity.Permission;
import com.behealthy.aaa.api.entity.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;


public class JwtTokenUtil {

    private static final Logger logger = LoggerFactory.getLogger(JwtTokenUtil.class);

    private final long EXPIRATION_TIMEOUT;

    private final String issuer;
    private final Algorithm algorithm;

    /**
     * @param privateKey optional param, required if you must generate tokens, else can pass null
     * @param publicKey  optional param, required if you must validate tokens, else can pass null
     */
    public JwtTokenUtil(RSAPrivateKey privateKey,
                        RSAPublicKey publicKey,
                        String algorithmName,
                        String issuer,
                        Long expirationTimeout) {
        this.algorithm = getAlgorithm(algorithmName, privateKey, publicKey);
        this.issuer = issuer;
        this.EXPIRATION_TIMEOUT = expirationTimeout;
    }

    public String generate(User user, List<Permission> permissions) {
        String token = null;
        List<String> claimPermissions = permissions.stream().map(Enum::toString).collect(Collectors.toList());
        Date issuedAt = new Date();
        Date expiresAt = new Date(issuedAt.getTime() + EXPIRATION_TIMEOUT);
        try {
            token = JWT.create()
                    .withIssuer(issuer)
                    .withIssuedAt(issuedAt)
                    .withExpiresAt(expiresAt)
                    .withClaim("name", user.getName())
                    .withClaim("permissions", claimPermissions)
                    .sign(algorithm);
        } catch (JWTCreationException exception) {
            logger.warn("can't generate token for this name = {} ",
                    user.getName(), exception);
        }
        return token;
    }

    public boolean verify(String token) {
        try {
            decodeToken(token);
        } catch (JWTVerificationException exception) {
            logger.warn("invalid token {}", token, exception);
            return false;
        }
        return true;
    }

    public boolean auth(String token, Permission permission) {
        List<String> permissions;
        try {
            DecodedJWT jwt = decodeToken(token);
            Claim claim = jwt.getClaim("permissions");
            permissions = claim.asList(String.class);
        } catch (JWTDecodeException exception) {
            logger.warn("invalid token {}", token, exception);
            return false;
        }
        return permissions.contains(permission.name());
    }

    private DecodedJWT decodeToken(String token) {
        JWTVerifier verifier = JWT.require(algorithm).build();
        return verifier.verify(token);
    }

    private Algorithm getAlgorithm(String algorithmName, RSAPrivateKey privateKey, RSAPublicKey publicKey) {
        switch (algorithmName) {
            case "RSA256":
                return Algorithm.RSA256(publicKey, privateKey);
            case "RSA384":
                return Algorithm.RSA384(publicKey, privateKey);
            case "RSA512":
                return Algorithm.RSA512(publicKey, privateKey);
            default:
                throw new IllegalArgumentException("Properties must contain correct name of algorithm");
        }
    }

}
