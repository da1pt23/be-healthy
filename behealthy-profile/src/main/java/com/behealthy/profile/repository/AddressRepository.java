package com.behealthy.profile.repository;

import com.behealthy.profile.entity.Address;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

@Repository
public class AddressRepository {

    private static final String SQL_INSERT_ADDRESS = "INSERT INTO address(id, profile_id ,city, street, flat, floor)" +
                                                     " values ( :id, :profileId, :city, :street, :flat, :floor)";

    private static final String SQL_UPDATE_ADDRESS = "UPDATE address SET" +
                                                    " profile_id = :profileId," +
                                                    " city = :city," +
                                                    " street = :street," +
                                                    " flat = :flat," +
                                                    " floor = :floor " +
                                                    " WHERE id = :id ";

    private static final String SQL_SELECT_ADDRESS_BY_ID = "SELECT * FROM address WHERE id = :id";

    private final AddressResultSetExtractor addressResultSetExtractor = new AddressResultSetExtractor();

    private final NamedParameterJdbcTemplate jdbc;

    public AddressRepository(NamedParameterJdbcTemplate jdbc) {
        this.jdbc = jdbc;
    }

    public void createAddress(Address address) {
        jdbc.update(SQL_INSERT_ADDRESS, new BeanPropertySqlParameterSource(address));
    }

    public void updateAddress(Address address) {
        jdbc.update(SQL_UPDATE_ADDRESS, new BeanPropertySqlParameterSource(address));
    }

    public Address getAddressById(UUID id) {
        List<Address> addresses =  jdbc.query(SQL_SELECT_ADDRESS_BY_ID, Collections.singletonMap("id",id),addressResultSetExtractor);
        if(addresses.size() > 0){
            return addresses.get(0);
        } else {
            return null;
        }
    }

    private static class AddressResultSetExtractor implements ResultSetExtractor<List<Address>> {

        @Override
        public List<Address> extractData(ResultSet rs) throws SQLException, DataAccessException {
            ArrayList<Address> result = new ArrayList<>();
            while (rs.next()) {
                Address address = new Address();

                address.setId((UUID) rs.getObject("id"));
                address.setProfileId((UUID) rs.getObject("profile_id"));
                address.setCity(rs.getString("city"));
                address.setStreet(rs.getString("street"));
                address.setFloor(rs.getString("floor"));
                address.setFlat(rs.getString("flat"));

                result.add(address);
            }
            return result;
        }
    }
}
