package com.behealthy.profile.service;

import com.behealthy.profile.entity.Address;
import com.behealthy.profile.entity.Profile;
import com.behealthy.profile.repository.ProfileRepository;
import org.springframework.stereotype.Service;
import org.springframework.util.IdGenerator;

import java.util.UUID;

@Service
public class ProfileService {
    private final AddressService addressService;
    private final ProfileRepository profileRepository;
    private final IdGenerator idGenerator;

    public ProfileService(AddressService addressService, ProfileRepository profileRepository, IdGenerator idGenerator) {
        this.addressService = addressService;
        this.profileRepository = profileRepository;
        this.idGenerator = idGenerator;
    }

    public Profile createProfile(Profile profile, Address address) {
        profile.setId(idGenerator.generateId());
        profileRepository.createProfile(profile);
        Profile createdProfile = profileRepository.getProfileByUsername(profile.getUsername());
        address.setProfileId(createdProfile.getId());
        addressService.createProfileAddress(address);
        return createdProfile;
    }

    public Profile getProfileByUsername(String id) {
        return profileRepository.getProfileByUsername(id);
    }

    public Profile getProfileById(UUID id) {
        return profileRepository.getProfileById(id);
    }

    public Profile updateProfile(Profile profile, Address address) {
        address.setProfileId(profile.getId());
        addressService.updateAddress(address);
        profileRepository.updateProfile(profile);
        return profileRepository.getProfileById(profile.getId());
    }

    public Boolean deleteProfile(UUID id) {
        return profileRepository.deleteProfile(id);
    }
}
