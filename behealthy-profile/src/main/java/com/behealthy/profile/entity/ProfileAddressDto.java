package com.behealthy.profile.entity;

public class ProfileAddressDto {
    private Address address;
    private Profile profile;

    public ProfileAddressDto(Profile profile, Address address) {
        this.address = address;
        this.profile = profile;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }
}
