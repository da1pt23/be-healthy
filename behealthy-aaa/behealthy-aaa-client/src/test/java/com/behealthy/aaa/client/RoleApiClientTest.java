package com.behealthy.aaa.client;

import com.behealthy.aaa.api.entity.Role;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.web.client.MockServerRestTemplateCustomizer;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;

import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

@RunWith(SpringRunner.class)
public class RoleApiClientTest {

    public static final String BASE_URL = "http://localhost:8083";
    private final RoleApiClient roleApiClient;
    private final MockServerRestTemplateCustomizer customizer;
    private final RestTemplateBuilder restTemplateBuilder;

    private static Role comparableRole;

    public RoleApiClientTest() {
        this.customizer = new MockServerRestTemplateCustomizer();
        this.restTemplateBuilder = new RestTemplateBuilder(customizer);
        this.roleApiClient = new RoleApiClient(BASE_URL, restTemplateBuilder);

    }

    @BeforeClass
    public static void initComparableRole() {

        comparableRole = new Role();
        comparableRole.setName("test");
    }


    @Test
    public void createRoleTest() {
        customizer.getServer().expect(requestTo("http://localhost:8083/role"))
                .andRespond(withSuccess("{\"name\":\"test\"}", MediaType.APPLICATION_JSON));
        Role role = roleApiClient.createRole(new Role());
        customizer.getServer().verify();
        Assert.assertEquals(comparableRole, role);
    }

    @Test
    public void getRoleTest() {
        customizer.getServer().expect(requestTo("http://localhost:8083/role/73d5e874-e28c-4f96-b898-a6aa5adb1ce2"))
                .andRespond(withSuccess("{\"name\":\"test\"}", MediaType.APPLICATION_JSON));
        Role createdRole = roleApiClient.getRoleById("73d5e874-e28c-4f96-b898-a6aa5adb1ce2");
        customizer.getServer().verify();
        Assert.assertEquals(comparableRole, createdRole);
    }

    @Test
    public void updateRoleTest() {
        Role role = new Role();
        role.setId("73d5e874-e28c-4f96-b898-a6aa5adb1ce2");
        customizer.getServer().expect(requestTo("http://localhost:8083/role/73d5e874-e28c-4f96-b898-a6aa5adb1ce2"))
                .andRespond(withSuccess("{\"name\":\"test\"}", MediaType.APPLICATION_JSON));

        Role createdRole = roleApiClient.updateRole(role);
        customizer.getServer().verify();
        Assert.assertEquals(comparableRole, createdRole);
    }

    @Test
    public void deleteRoleTest() {
        customizer.getServer().expect(requestTo("http://localhost:8083/role/73d5e874-e28c-4f96-b898-a6aa5adb1ce2"))
                .andRespond(withSuccess("true", MediaType.APPLICATION_JSON));
        roleApiClient.deleteRole("73d5e874-e28c-4f96-b898-a6aa5adb1ce2");
        customizer.getServer().verify();
    }
}
