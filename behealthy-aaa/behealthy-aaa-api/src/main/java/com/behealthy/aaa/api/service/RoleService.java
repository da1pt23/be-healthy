package com.behealthy.aaa.api.service;

import com.behealthy.aaa.api.entity.Role;

public interface RoleService {
    Role createRole(Role role);

    Role getRoleById(String roleId);

    Role updateRole(Role role);

    void deleteRole(String id);
}
