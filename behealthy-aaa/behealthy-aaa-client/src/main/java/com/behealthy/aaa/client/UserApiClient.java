package com.behealthy.aaa.client;

import com.behealthy.aaa.api.entity.User;
import com.behealthy.aaa.api.service.UserService;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

public class UserApiClient implements UserService {

    private final static String PATH_USER = "/user";

    private final String url;
    private final RestTemplate restTemplate;

    public UserApiClient(String baseUrl, RestTemplateBuilder restTemplateBuilder) {
        this.url = baseUrl + PATH_USER;
        this.restTemplate = restTemplateBuilder.build();
    }

    @Override
    public User createUser(User user) {
        HttpEntity<User> entity = new HttpEntity<>(user);
        ResponseEntity<User> responseEntity = restTemplate.exchange(url, HttpMethod.POST, entity, User.class);
        return responseEntity.getBody();
    }

    @Override
    public User getUserById(String userId) {
        User user;
        try {
            ResponseEntity<User> responseEntity = restTemplate.getForEntity(url + "/{userId}", User.class, userId);
            user = responseEntity.getBody();
        } catch (HttpClientErrorException.NotFound ex) {
            user = null;
        }
        return user;
    }

    @Override
    public User updateUser(User user) {
        HttpEntity<User> entity = new HttpEntity<>(user);
        ResponseEntity<User> responseEntity = restTemplate.exchange(url + "/{userId}", HttpMethod.PUT, entity, User.class, user.getId());
        return responseEntity.getBody();
    }

    @Override
    public void deleteUser(String userId) {
        restTemplate.delete(url + "/{userId}", userId);
    }
}
