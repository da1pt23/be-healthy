package com.behealthy.aaa.rest.controller;

import com.behealthy.aaa.api.entity.RequiredPermissions;
import com.behealthy.aaa.api.entity.Role;
import com.behealthy.aaa.api.service.RoleService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;

import static com.behealthy.aaa.api.entity.Permission.*;

@RestController
@RequestMapping(value = "/role")
public class RoleController {

    private final RoleService roleService;

    public RoleController(RoleService roleService) {
        this.roleService = roleService;
    }


    @RequiredPermissions(ROLE_ADD)
    @PostMapping
    public ResponseEntity<Role> createRole(@RequestBody Role role) {
        return new ResponseEntity<>(roleService.createRole(role), HttpStatus.OK);
    }

    @RequiredPermissions(ROLE_GET)
    @GetMapping(value = "/{roleId}")
    public ResponseEntity<Role> getRoleById(@PathVariable String roleId) {
        Role role = roleService.getRoleById(roleId);
        if (role == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(role, HttpStatus.OK);
        }
    }

    @RequiredPermissions(ROLE_UPDATE)
    @PutMapping(value = "/{roleId}")
    private ResponseEntity<Role> updateRole(@RequestBody Role role, @PathVariable String roleId) {
        if (!Objects.equals(role.getId(), roleId)) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        if (role.getName() != null) {
            return new ResponseEntity<>(roleService.updateRole(role), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @RequiredPermissions(ROLE_DELETE)
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Void> deleteRole(@PathVariable String id) {
        roleService.deleteRole(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
