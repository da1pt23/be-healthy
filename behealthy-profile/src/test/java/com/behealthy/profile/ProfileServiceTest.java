package com.behealthy.profile;

import com.behealthy.profile.entity.Address;
import com.behealthy.profile.entity.Profile;
import com.behealthy.profile.repository.ProfileRepository;
import com.behealthy.profile.service.AddressService;
import com.behealthy.profile.service.ProfileService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.util.IdGenerator;

import java.time.Instant;
import java.util.Date;
import java.util.UUID;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ProfileServiceTest {

    //PROFILE
    private static final String ID = "633b9cec-ab1d-11ea-bb37-0242ac130002";
    private static final String BIRTHDAY = "2000-01-01T10:10:10.10Z";
    private static final String PHONE = "+380667788933";
    private static final String EMAIL = "john@behealhy.com";
    private static final String USERNAME = "JOHN";

    //Address
    private static final String ADDRESS_ID = "6351f43e-ab21-11ea-bb37-0242ac130002";
    private static final String CITY = "Montreal";
    private static final String STREET = "Saint-Luc";
    private static final String FLOOR = "15";
    private static final String FLAT = "153a";

    @InjectMocks
    private ProfileService profileService;

    @Mock
    private AddressService addressService;
    @Mock
    private ProfileRepository profileRepository;
    @Mock
    private IdGenerator idGenerator;

    private final Profile profile = new Profile();
    private final Address createdAddress = new Address();
    private final Address addressWithoutId = new Address();

    @Before
    public void init() {
        profile.setBirthday(Date.from(Instant.parse(BIRTHDAY)));
        profile.setPhone(PHONE);
        profile.setEmail(EMAIL);
        profile.setUsername(USERNAME);

        addressWithoutId.setCity(CITY);
        addressWithoutId.setStreet(STREET);
        addressWithoutId.setFloor(FLOOR);
        addressWithoutId.setFlat(FLAT);

        createdAddress.setId(UUID.fromString(ADDRESS_ID));
        createdAddress.setCity(CITY);
        createdAddress.setStreet(STREET);
        createdAddress.setFloor(FLOOR);
        createdAddress.setFlat(FLAT);
    }

    @Test
    public void createProfileTest() {
        when(idGenerator.generateId()).thenReturn(UUID.fromString(ID));
        when(profileRepository.getProfileByUsername(USERNAME)).thenReturn(profile);
        when(addressService.createProfileAddress(addressWithoutId)).thenReturn(createdAddress);
        Profile createdProfile = profileService.createProfile(profile, addressWithoutId);
        Assert.assertNotNull(createdProfile.getId());
    }

    @Test
    public void getProfileByUsername() {
        profile.setId(UUID.fromString(ID));
        when(profileRepository.getProfileByUsername(USERNAME)).thenReturn(profile);
        Profile profile = profileService.getProfileByUsername(USERNAME);
        Assert.assertEquals(profile, this.profile);
    }

    @Test
    public void getProfileById() {
        profile.setId(UUID.fromString(ID));
        when(profileRepository.getProfileById(UUID.fromString(ID))).thenReturn(profile);
        Profile profile = profileService.getProfileById(UUID.fromString(ID));
        Assert.assertEquals(profile, this.profile);
    }

    @Test
    public void updateProfileTest() {
        createdAddress.setFlat("1000");
        profile.setUsername("George Floyd");
        profile.setId(UUID.fromString(ID));
        when(profileRepository.getProfileById(UUID.fromString(ID))).thenReturn(profile);
        Profile updatedProfile = profileService.updateProfile(profile, createdAddress);
        Assert.assertEquals(createdAddress.getFlat(),"1000");
        Assert.assertEquals(updatedProfile.getUsername(),"George Floyd");
    }

}
