package com.behealthy.aaa.rest.repository.mapper;

import com.behealthy.aaa.api.entity.Permission;
import com.behealthy.aaa.api.entity.Role;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class RoleResultSetExtractor implements ResultSetExtractor<List<Role>> {

    @Override
    public List<Role> extractData(ResultSet resultSet) throws SQLException, DataAccessException {
        List<Role> result = new ArrayList<>();
        Role role = null;
        while (resultSet.next()) {
            String id = resultSet.getString("id");
            if (role == null || (!role.getId().equals(id))) {
                role = new Role();
                result.add(role);
            }
            role.setId(resultSet.getString("id"));
            role.setName(resultSet.getString("name"));
            String permission = resultSet.getString("permission");
            if (permission != null) {
                role.getPermissions().add(Permission.valueOf(permission));
            }
            result.add(role);
        }
        return result;
    }
}
