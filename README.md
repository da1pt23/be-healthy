# Be Healthy Project
### For using service, you must set up 2 microservices: 
- behealthy-aaa-rest.
- behealthy-web.
All requests must call behealthy-web which use behealthy-aaa-rest 

### How to set up behealthy-aaa-rest
1)Create a postgres database and run in it - schema.sql and data.sql files, which places in behealthy-aaa-rest/src/main/resources.
Database name must be behealthy, username=postgres, password=postgres. If you have some problems you could
override default name,username and password in behealthy-aaa/behealthy-aaa-rest/config/application-local.yaml
    
2)When you running behealthy-aaa-rest you must set this property

      spring.profiles.active=local

###How to set up behealthy-web
       
1)When you running behealthy-aaa-rest you must set this property

      spring.profiles.active=local
