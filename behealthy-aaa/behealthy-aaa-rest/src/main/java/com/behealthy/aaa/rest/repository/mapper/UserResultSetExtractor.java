package com.behealthy.aaa.rest.repository.mapper;

import com.behealthy.aaa.api.entity.User;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserResultSetExtractor implements ResultSetExtractor<List<User>> {

    @Override
    public List<User> extractData(ResultSet resultSet) throws SQLException, DataAccessException {
        List<User> result = new ArrayList<>();
        User user = null;
        while (resultSet.next()) {
            String id = resultSet.getString("id");
            if (user == null || (!user.getId().equals(id))) {
                user = new User();
                result.add(user);
            }
            user.setId(resultSet.getString("id"));
            user.setName(resultSet.getString("username"));
            user.setPassword(resultSet.getString("password"));
            String roleId = resultSet.getString("role_id");
            if (roleId != null) {
                user.getRoles().add(roleId);
            }
        }
        return result;
    }
}
