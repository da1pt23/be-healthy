package com.behealthy.aaa.api.service;

import com.behealthy.aaa.api.entity.User;

/**
 * Generates a token for {@code User}
 */
public interface AuthService {

    /**
     * Checks user credentials and then generates token
     * @param user for which generates token
     * @return token
     */
    String auth(User user);
}
