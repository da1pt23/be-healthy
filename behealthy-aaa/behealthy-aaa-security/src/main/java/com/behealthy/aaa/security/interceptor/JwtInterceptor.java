package com.behealthy.aaa.security.interceptor;

import com.behealthy.aaa.api.entity.Permission;
import com.behealthy.aaa.api.entity.RequiredPermissions;
import com.behealthy.aaa.api.service.TokenService;
import com.behealthy.aaa.security.jwt.JwtTokenUtil;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;

public class JwtInterceptor implements HandlerInterceptor {

    private final JwtTokenUtil jwtTokenUtil;
    private final TokenService tokenService;

    public JwtInterceptor(JwtTokenUtil jwtTokenUtil, TokenService tokenService) {
        this.jwtTokenUtil = jwtTokenUtil;
        this.tokenService = tokenService;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        Permission methodRequiredPermission = extractRequiredPermission(handler);
        if (methodRequiredPermission != null) {
            String jwtToken = request.getHeader(HttpHeaders.AUTHORIZATION);
            if (jwtToken != null && jwtTokenUtil.auth(jwtToken, methodRequiredPermission) && !tokenService.isValidToken(jwtToken)) {
                return true;
            } else {
                response.setStatus(HttpStatus.UNAUTHORIZED.value());
                return false;
            }
        } else {
            return true;
        }
    }

    private Permission extractRequiredPermission(Object handler) {
        if (handler instanceof HandlerMethod) {
            HandlerMethod handlerMethod = (HandlerMethod) handler;
            Method method = handlerMethod.getMethod();
            if (method.isAnnotationPresent(RequiredPermissions.class)) {
                return method.getAnnotation(RequiredPermissions.class).value();
            } else {
                return null;
            }
        } else {
            return null;
        }
    }
}
