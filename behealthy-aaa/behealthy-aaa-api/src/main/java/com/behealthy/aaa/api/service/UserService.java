package com.behealthy.aaa.api.service;

import com.behealthy.aaa.api.entity.User;

public interface UserService {
    User createUser(User user);

    User getUserById(String userId);

    User updateUser(User user);

    void deleteUser(String id);

}