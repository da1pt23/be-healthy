package com.behealthy.profile.repository;

import com.behealthy.profile.entity.Profile;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

@Repository
public class ProfileRepository {

    private static final String SQL_INSERT_PROFILE = "INSERT INTO profile(id,username,email,phone)" +
            " VALUES (:id,:username,:email,:phone)";

    private static final String SQL_SELECT_PROFILE_BY_ID = "SELECT * FROM profile " +
            " WHERE id = :id";

    private static final String SQL_SELECT_PROFILE_BY_USERNAME = "SELECT * FROM profile " +
            " WHERE username = :username";

    private static final String SQL_UPDATE_PROFILE = "UPDATE profile " +
            "SET " +
            "username = :username, " +
            "email = :email, " +
            "phone = :phone " +
            "WHERE id = :id";

    private static final String SQL_DELETE_PROFILE = "DELETE FROM profile WHERE id = :id";

    private static final ProfileResultSetExtractor PROFILE_RESULT_SET_EXTRACTOR = new ProfileResultSetExtractor();

    private final NamedParameterJdbcTemplate jdbc;


    public ProfileRepository(NamedParameterJdbcTemplate jdbc) {
        this.jdbc = jdbc;
    }

    public boolean createProfile(Profile profile) {
        return jdbc.update(SQL_INSERT_PROFILE, new BeanPropertySqlParameterSource(profile)) > 0;
    }

    public Profile getProfileByUsername(String username) {
        List<Profile> profiles = jdbc.query(SQL_SELECT_PROFILE_BY_USERNAME, Collections
                .singletonMap("username", username), PROFILE_RESULT_SET_EXTRACTOR);
        if (profiles.size() > 0) {
            return profiles.get(0);
        } else {
            return null;
        }
    }

    public Profile getProfileById(UUID id) {
        List<Profile> profiles = jdbc.query(SQL_SELECT_PROFILE_BY_ID, Collections
                .singletonMap("id", id), PROFILE_RESULT_SET_EXTRACTOR);
        if (profiles.size() > 0) {
            return profiles.get(0);
        } else {
            return null;
        }
    }

    public void updateProfile(Profile profile) {
        jdbc.update(SQL_UPDATE_PROFILE, new BeanPropertySqlParameterSource(profile));
    }

    public Boolean deleteProfile(UUID id) {
        return jdbc.update(SQL_DELETE_PROFILE, Collections.singletonMap("id", id)) > 0;
    }

    private static class ProfileResultSetExtractor implements ResultSetExtractor<List<Profile>> {
        @Override
        public List<Profile> extractData(ResultSet rs) throws SQLException, DataAccessException {
            ArrayList<Profile> result = new ArrayList<>();
            while (rs.next()) {
                Profile profile = new Profile();

                profile.setId((UUID) rs.getObject("id"));
                profile.setUsername(rs.getString("username"));
                profile.setBirthday(rs.getDate("birthday"));
                profile.setEmail(rs.getString("email"));
                profile.setPhone(rs.getString("phone"));

                result.add(profile);
            }
            return result;
        }
    }

}
