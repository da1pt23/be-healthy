create table "users"
(
    id       VARCHAR(36) PRIMARY KEY,
    username VARCHAR(36) UNIQUE,
    password varchar(60)
);

create table "role"
(
    id   VARCHAR(36) PRIMARY KEY,
    name VARCHAR(36) UNIQUE
);

create table "role_permissions"
(
    role_id    VARCHAR(36),
    permission VARCHAR(50),
    FOREIGN KEY (role_id) REFERENCES role (id) ON DELETE CASCADE
);

create table user_roles
(
    user_id VARCHAR(36),
    role_id VARCHAR(36),
    FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE,
    FOREIGN KEY (role_id) REFERENCES role (id) ON DELETE CASCADE
);

CREATE TABLE jwt_black_list
(
    jwt_token varchar(2048) UNIQUE NOT NULL
)
