package com.behealthy.aaa.client;

import com.behealthy.aaa.api.entity.Role;
import com.behealthy.aaa.api.service.RoleService;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

public class RoleApiClient implements RoleService {

    private final static String PATH_ROLE = "/role";

    private final String baseUrl;
    private final RestTemplate restTemplate;


    public RoleApiClient(String baseUrl, RestTemplateBuilder restTemplateBuilder) {
        this.baseUrl = baseUrl + PATH_ROLE;
        this.restTemplate = restTemplateBuilder.build();
    }

    @Override
    public Role createRole(Role role) {
        HttpEntity<Role> entity = new HttpEntity<>(role);
        ResponseEntity<Role> responseEntity = restTemplate.exchange(baseUrl, HttpMethod.POST, entity, Role.class);
        return responseEntity.getBody();
    }

    @Override
    public Role getRoleById(String roleId) {
        Role role;
        try {
            ResponseEntity<Role> responseEntity = restTemplate.getForEntity(baseUrl + "/{roleId}",  Role.class, roleId);
            role = responseEntity.getBody();
        } catch (HttpClientErrorException.NotFound ex) {
            role = null;
        }
        return role;
    }

    @Override
    public Role updateRole(Role role) {
        HttpEntity<Role> entity = new HttpEntity<>(role);
        ResponseEntity<Role> responseEntity = restTemplate.exchange(baseUrl + "/{roleId}", HttpMethod.PUT, entity, Role.class, role.getId());
        return responseEntity.getBody();

    }

    @Override
    public void deleteRole(String id) {
        restTemplate.delete(baseUrl + "/{id}", id);
    }
}
