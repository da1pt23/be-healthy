package com.behealthy.profile.controller;

import com.behealthy.profile.entity.Address;
import com.behealthy.profile.entity.Profile;
import com.behealthy.profile.entity.ProfileAddressDto;
import com.behealthy.profile.service.ProfileService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.UUID;

@RestController
@RequestMapping("/profile")
public class ProfileController {

    ProfileService profileService;

    public ProfileController(ProfileService profileService) {
        this.profileService = profileService;
    }

    @PostMapping
    public ResponseEntity<Profile> createProfile(@RequestBody ProfileAddressDto profileAddressDto) {
        Profile createdProfile = profileService.createProfile(profileAddressDto.getProfile(),
                profileAddressDto.getAddress());
        return new ResponseEntity<>(createdProfile, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Profile> getProfileById(@PathVariable UUID id) {
        Profile profile = profileService.getProfileById(id);
        if (profile == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(profile, HttpStatus.OK);
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<Profile> updateProfile(@RequestBody ProfileAddressDto profileAddressDto, @PathVariable UUID id) {
        Profile profile = profileAddressDto.getProfile();
        Address address = profileAddressDto.getAddress();
        if (!Objects.equals(profile.getId(), id)) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        Profile updatedProfile = profileService.updateProfile(profile, address);
        if (updatedProfile == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(updatedProfile, HttpStatus.OK);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteProfile(@PathVariable UUID id) {
        profileService.deleteProfile(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
