package com.behealthy.aaa.rest.controller;

import com.behealthy.aaa.api.entity.Permission;
import com.behealthy.aaa.api.entity.RequiredPermissions;
import com.behealthy.aaa.api.service.TokenService;
import com.behealthy.aaa.rest.service.JdbcTokenService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TokenController {

    private final TokenService tokenService;

    public TokenController(JdbcTokenService tokenService) {
        this.tokenService = tokenService;
    }

    @RequiredPermissions(Permission.TOKEN_CHECK)
    @PostMapping("/token/check")
    public ResponseEntity<Boolean> isValidToken(@RequestBody String token) {
        return new ResponseEntity<>(tokenService.isValidToken(token), HttpStatus.OK);
    }

    @RequiredPermissions(Permission.TOKEN_INVALIDATE)
    @PostMapping(value = "/token/invalidate")
    public ResponseEntity<Void> invalidateToken(@RequestBody String token) {
        tokenService.invalidateToken(token);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
