package com.behealthy.aaa.rest.repository;

import com.behealthy.aaa.api.entity.Permission;
import com.behealthy.aaa.api.entity.Role;
import com.behealthy.aaa.rest.repository.mapper.RoleResultSetExtractor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Repository
public class RoleRepository {
    private static final String SQL_INSERT_ROLE = "INSERT INTO role( id, name ) values ( :id, :name ) ";

    private static final String SQL_SELECT_ROLE_BY_ID =
            "SELECT" +
                    " id," +
                    " name," +
                    " permission" +
                    " FROM role AS r" +
                    " LEFT JOIN role_permissions AS rp" +
                    " ON  r.id = rp.role_id" +
                    " WHERE" +
                    " id = :roleId";

    private static final String SQL_UPDATE_ROLE = "UPDATE role SET name = :name WHERE id = :id";

    private static final String SQL_DELETE_ROLE = "DELETE FROM role WHERE id = :userId ";

    private static final String SQL_SELECT_ROLE_PERMISSIONS = "SELECT permission FROM role_permissions WHERE role_id = :roleId";

    private static final String SQL_INSERT_USER_ROLES = "INSERT INTO user_roles values ( :userId , :roleId ) ";

    private static final String SQL_DELETE_USER_ROLES = "DELETE FROM user_roles WHERE user_id = :userId ";

    private static final RoleResultSetExtractor ROLE_RESULT_SET_EXTRACTOR = new RoleResultSetExtractor();

    private final NamedParameterJdbcTemplate jdbc;

    @Autowired
    public RoleRepository(NamedParameterJdbcTemplate jdbc) {
        this.jdbc = jdbc;
    }

    public void createRole(Role role) {
        jdbc.update(SQL_INSERT_ROLE, new BeanPropertySqlParameterSource(role));
    }

    public Role getRoleById(String roleId) {
        List<Role> roles = jdbc.query(SQL_SELECT_ROLE_BY_ID, Collections.singletonMap("roleId", roleId), ROLE_RESULT_SET_EXTRACTOR);
        if (roles == null || roles.isEmpty()) {
            return null;
        } else {
            return roles.get(0);
        }
    }

    public List<Permission> getRolePermissions(String roleId) {
        return jdbc.query(
                SQL_SELECT_ROLE_PERMISSIONS, Collections.singletonMap("roleId", roleId),
                (rs, rowNum) -> Permission.valueOf(rs.getString("permission")));
    }

    public boolean updateRole(Role role) {
        return jdbc.update(SQL_UPDATE_ROLE, new BeanPropertySqlParameterSource(role)) > 0;
    }

    public void deleteRole(String id) {
        jdbc.update(SQL_DELETE_ROLE, Collections.singletonMap("userId", id));
    }

    public boolean saveUserRoles(String userId, List<String> roleIds) {
        List<MapSqlParameterSource> batchArgs = new ArrayList<>();
        for (String roleId : roleIds) {
            MapSqlParameterSource parameters = new MapSqlParameterSource();
            parameters.addValue("userId", userId);
            parameters.addValue("roleId", roleId);
            batchArgs.add(parameters);
        }
        return jdbc.batchUpdate(SQL_INSERT_USER_ROLES, batchArgs.toArray(new MapSqlParameterSource[roleIds.size()])).length > 0;
    }

    public void deleteUserRoles(String userId) {
        jdbc.update(SQL_DELETE_USER_ROLES, Collections.singletonMap("userId", userId));
    }
}
