package com.behealthy.web.service;

import com.behealthy.aaa.api.entity.User;
import com.behealthy.aaa.api.service.AuthService;
import com.behealthy.aaa.api.service.TokenService;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

@Service
public class LoginService {

    private final TokenService tokenService;
    private final AuthService authService;

    public LoginService(TokenService tokenService, AuthService authService) {
        this.tokenService = tokenService;
        this.authService = authService;
    }

    /**
     * @param user which auth
     * @return token if user successful authorized, else return null
     */
    public String auth(@RequestBody User user) {
        return authService.auth(user);
    }

    public void logout(String token) {
        tokenService.invalidateToken(token);
    }
}
