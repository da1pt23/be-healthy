import com.auth0.jwt.exceptions.TokenExpiredException;
import com.behealthy.aaa.api.entity.Permission;
import com.behealthy.aaa.api.entity.User;
import com.behealthy.aaa.security.jwt.JwtTokenUtil;
import com.behealthy.aaa.security.jwt.PemKeyReader;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.junit4.SpringRunner;

import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(properties = "jwt.token.expiration.timeout = 0")
public class JwtTokenUtilTest {

    private static final String USER_NAME = "TU-";

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    private User user;
    private List<Permission> permissions;

    @Before
    public void init() {
        User user = new User();
        user.setName(USER_NAME + (int) (Math.random() * 1000));
        this.user = user;

        List<Permission> permissions = new ArrayList<>();
        permissions.add(Permission.ROLE_ADD);
        this.permissions = permissions;
    }

    @Test
    public void expiredToken() {
        String token = jwtTokenUtil.generate(user, permissions);
        Permission requiredPermission = permissions.get(0);
        boolean isTokenExpired = false;
        try {
            Thread.sleep(1000);
            jwtTokenUtil.auth(token, requiredPermission);
        } catch (TokenExpiredException | InterruptedException ex) {
            Assert.assertTrue(ex.getMessage().contains("The Token has expired"));
            isTokenExpired = true;
        }
        Assert.assertTrue(isTokenExpired);
    }

    @Configuration
    public static class TestConfiguration {

        @Bean
        public JwtTokenUtil jwtTokenUtil(@Value("${jwt.path.key.public}") String publicKeyFilePath,
                                         @Value("${jwt.path.key.private}") String privateKeyFilePath,
                                         @Value("${jwt.algorithm.type}") String algorithmType,
                                         @Value("${jwt.algorithm.name}") String algorithmName,
                                         @Value("${jwt.issuer}") String issuer,
                                         @Value("${jwt.token.expiration.timeout}") Long tokenExpirationTimeout) throws Exception {
            RSAPrivateKey privateKey = PemKeyReader.readPrivateKeyFromFile(privateKeyFilePath, algorithmType);
            RSAPublicKey publicKey = PemKeyReader.readPublicKeyFromFile(publicKeyFilePath, algorithmType);
            return new JwtTokenUtil(privateKey, publicKey, algorithmName, issuer, tokenExpirationTimeout);
        }
    }

}
