package com.behealthy.aaa.rest.repository;

import com.behealthy.aaa.api.entity.User;
import com.behealthy.aaa.rest.repository.mapper.UserResultSetExtractor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.Collections;
import java.util.List;

@Repository
public class UserRepository {

    private static final String SQL_CREATE_USER = "INSERT INTO users (id, username, password) values (:id, :name, :password)";

    private static final String SQL_READ_USER_BY_ID =
            "SELECT" +
                    " id," +
                    " username," +
                    " password," +
                    " ur.role_id " +
                    "FROM" +
                    " users AS u" +
                    " LEFT JOIN user_roles AS ur ON u.id = ur.user_id " +
                    "WHERE" +
                    " id = :id";

    private static final String SQL_READ_USER_BY_NAME =
            "SELECT" +
                    " id," +
                    " username," +
                    " password," +
                    " ur.role_id " +
                    "FROM" +
                    " users AS u" +
                    " LEFT JOIN user_roles AS ur ON u.id = ur.user_id " +
                    "WHERE" +
                    " username = :name";

    private static final String SQL_UPDATE_USER_NAME = "UPDATE users SET username = :name WHERE id = :id";

    private static final String SQL_UPDATE_USER_PASSWORD = "UPDATE users SET password = :password WHERE id = :id";

    private static final String SQL_DELETE_USER = "DELETE FROM users WHERE id = :id";

    private static final UserResultSetExtractor USER_RESULT_SET_EXTRACTOR = new UserResultSetExtractor();

    private final NamedParameterJdbcTemplate jdbc;

    @Autowired
    public UserRepository(NamedParameterJdbcTemplate jdbc) {
        this.jdbc = jdbc;
    }

    public void createUser(User user) {
        jdbc.update(SQL_CREATE_USER, new BeanPropertySqlParameterSource(user));
    }

    public User getUserById(String userId) {
        List<User> users = jdbc.query(SQL_READ_USER_BY_ID, Collections.singletonMap("id", userId), USER_RESULT_SET_EXTRACTOR);
        if (users == null || users.isEmpty()) {
            return null;
        } else {
            return users.get(0);
        }
    }

    public User getUserByName(String name) {
        List<User> users = jdbc.query(SQL_READ_USER_BY_NAME, Collections.singletonMap("name", name), USER_RESULT_SET_EXTRACTOR);
        if (users == null || users.isEmpty()) {
            return null;
        } else {
            return users.get(0);
        }
    }

    public void update(User user) {
        jdbc.update(SQL_UPDATE_USER_NAME, new BeanPropertySqlParameterSource(user));
    }

    public void updateUserPassword(User user) {
        jdbc.update(SQL_UPDATE_USER_PASSWORD, new BeanPropertySqlParameterSource(user));
    }

    public void deleteUser(String id) {
        jdbc.update(SQL_DELETE_USER, Collections.singletonMap("id", id));
    }

}
