package com.behealthy.profile;

import com.behealthy.profile.entity.Address;
import com.behealthy.profile.repository.AddressRepository;
import com.behealthy.profile.service.AddressService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.util.IdGenerator;

import java.util.UUID;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AddressServiceTest {

    //Address
    private static final String ADDRESS_ID = "6351f43e-ab21-11ea-bb37-0242ac130002";
    private static final String CITY = "Montreal";
    private static final String STREET = "Saint-Luc";
    private static final String FLOOR = "15";
    private static final String FLAT = "153a";

    @InjectMocks
    private AddressService addressService;

    @Mock
    private IdGenerator idGenerator;
    @Mock
    private AddressRepository addressRepository;

    private final Address address = new Address();

    @Before
    public void init() {
        address.setCity(CITY);
        address.setStreet(STREET);
        address.setFloor(FLOOR);
        address.setFlat(FLAT);
    }


    @Test
    public void createAddress() {
        when(idGenerator.generateId()).thenReturn(UUID.fromString(ADDRESS_ID));
        Address createdAddress = addressService.createProfileAddress(address);
        Assert.assertEquals(createdAddress.getId(),UUID.fromString(ADDRESS_ID));
    }
}
