package com.behealthy.aaa.rest.service;

import com.behealthy.aaa.api.entity.Permission;
import com.behealthy.aaa.api.entity.User;
import com.behealthy.aaa.security.jwt.JwtTokenUtil;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class JwtAuthServiceTest {

    private static final String USER_NAME = "TestUser";
    private static final String USER_PASSWORD = "1111";
    private static final String UGLY_ENCRYPTED_PASSWORD = "$2a$10$vHyD9YGNBRUw6YNrEMM1/uL4";

    @InjectMocks
    private JwtAuthService jwtAuthService;

    @Mock
    private JwtTokenUtil jwtTokenUtil;
    @Mock
    private JdbcUserService jdbcUserService;
    @Mock
    private PasswordEncoder passwordEncoder;

    private User registeredUser;
    private User user;

    @Before
    public void initUser() {
        registeredUser = new User();
        registeredUser.setName(USER_NAME);
        registeredUser.setPassword(UGLY_ENCRYPTED_PASSWORD);

        user = new User();
        user.setName(USER_NAME);
        user.setPassword(USER_PASSWORD);
    }

    @Test
    public void successfulAuthTest() {
        List<Permission> permissions = new ArrayList<>();
        when(jdbcUserService.getUserByName(USER_NAME)).thenReturn(registeredUser);
        when(passwordEncoder.matches(USER_PASSWORD, UGLY_ENCRYPTED_PASSWORD)).thenReturn(true);
        when(jwtTokenUtil.generate(registeredUser, permissions)).thenReturn("verySecretToken");
        String token = jwtAuthService.auth(user);
        verify(jwtTokenUtil).generate(registeredUser, permissions);
        Assert.assertEquals("verySecretToken", token);
    }

    @Test
    public void unsuccessfulAuthTest(){
        List<Permission> permissions = new ArrayList<>();
        when(jdbcUserService.getUserByName(USER_NAME)).thenReturn(registeredUser);
        when(passwordEncoder.matches(USER_PASSWORD, UGLY_ENCRYPTED_PASSWORD)).thenReturn(false);

        String token = jwtAuthService.auth(user);
        verify(jwtTokenUtil, never()).generate(registeredUser, permissions);
        Assert.assertNull(token);
    }

}
