package com.behealthy.aaa.client;

import com.behealthy.aaa.api.entity.User;
import com.behealthy.aaa.api.service.AuthService;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class AuthApiClient implements AuthService {

    private static final String AUTH = "/auth";

    private final String baseUrl;
    private final RestTemplate restTemplate;

    public AuthApiClient(String baseUrl, RestTemplateBuilder restTemplateBuilder) {
        this.baseUrl = baseUrl;
        this.restTemplate = restTemplateBuilder.build();
    }

    @Override
    public String auth(User user) {
        HttpEntity<User> entity = new HttpEntity<>(user);
        ResponseEntity<String> response = restTemplate.exchange(baseUrl + AUTH, HttpMethod.POST, entity, String.class);
        if (response.getStatusCode() == HttpStatus.OK) {
            return response.getBody();
        }
        return null;
    }
}
