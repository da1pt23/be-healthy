package com.behealthy.aaa.rest.service;

import com.behealthy.aaa.api.entity.Role;
import com.behealthy.aaa.rest.repository.RoleRepository;
import com.behealthy.aaa.rest.service.util.IdGenerator;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class JdbcRoleServiceTest {

    @InjectMocks
    private JdbcRoleService roleService;

    @Mock
    private RoleRepository roleRepository;
    @Mock
    private IdGenerator idGenerator;

    private Role role;

    @Before
    public void initRole() {
        Role role = new Role();
        role.setId("1");
        role.setName("testRole");
        this.role = role;
    }

    @Test
    public void createRole() {
        when(idGenerator.generateId()).thenReturn("1");
        when(roleRepository.getRoleById("1")).thenReturn(role);
        Role createdRole = roleService.createRole(new Role());
        Assert.assertEquals(role, createdRole);
        Assert.assertEquals(role.getId(), createdRole.getId());
    }
}
