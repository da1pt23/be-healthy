package com.behealthy.aaa.rest.repository;

import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.Collections;

@Repository
public class TokenRepository {

    private static final String SQL_SELECT_JWT_TOKEN = "SELECT COUNT(*) > 0 FROM jwt_black_list where jwt_token = :jwtToken";

    private static final String SQL_INSERT_JWT_TOKEN =
            " INSERT INTO jwt_black_list( jwt_token) VALUES(:jwtToken)" +
            " ON CONFLICT ON CONSTRAINT jwt_black_list_jwt_token_key" +
            " DO NOTHING;";

    private final NamedParameterJdbcTemplate jdbcTemplate;

    public TokenRepository(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public boolean isValidToken(String token) {
        return jdbcTemplate.queryForObject(SQL_SELECT_JWT_TOKEN, Collections.singletonMap("jwtToken", token), Boolean.class);
    }

    public void invalidateToken(String token) {
        jdbcTemplate.update(SQL_INSERT_JWT_TOKEN, Collections.singletonMap("jwtToken", token));
    }

}
