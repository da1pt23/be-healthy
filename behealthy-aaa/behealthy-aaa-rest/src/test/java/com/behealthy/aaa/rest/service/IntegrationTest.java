package com.behealthy.aaa.rest.service;

import com.behealthy.aaa.api.entity.Role;
import com.behealthy.aaa.api.entity.User;
import com.behealthy.aaa.api.service.RoleService;
import com.behealthy.aaa.api.service.UserService;
import com.behealthy.aaa.client.RoleApiClient;
import com.behealthy.aaa.client.UserApiClient;
import com.behealthy.aaa.rest.AaaRestApplication;
import com.behealthy.aaa.rest.service.configuration.RestTemplateConfiguration;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@TestPropertySource(locations = {"/application.properties"})
@SpringBootTest(classes = {AaaRestApplication.class, RestTemplateConfiguration.class},
                    webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class IntegrationTest {

    @Autowired
    private RestTemplateBuilder restTemplateBuilder;

    @LocalServerPort
    private int port;

    private UserService userService;
    private RoleService roleService;

    @Before
    public void init() {
        String url = "http://localhost:" + port;

        this.userService = new UserApiClient(url, restTemplateBuilder);
        this.roleService = new RoleApiClient(url, restTemplateBuilder);
    }

    @Test
    public void userWorkflowTest() {
        User newUser = new User();
        newUser.setName("NewUser");
        newUser.setPassword("1111");

        User createdUser = userService.createUser(newUser);
        Assert.assertNull(createdUser.getPassword());
        Assert.assertNotNull(createdUser.getId());
        newUser.setId(createdUser.getId());
        newUser.setPassword(null);
        Assert.assertEquals(newUser, createdUser);

        User selectedUser = userService.getUserById(createdUser.getId());
        Assert.assertNotNull(selectedUser.getId());
        Assert.assertNull(selectedUser.getPassword());
        Assert.assertEquals(createdUser, selectedUser);

        createdUser.setName("UpdateUser");
        createdUser.setPassword("2222");
        User updatedUser = userService.updateUser(createdUser);
        updatedUser.setPassword("2222");
        Assert.assertEquals(createdUser, updatedUser);

        userService.deleteUser(updatedUser.getId());
        Assert.assertNull(userService.getUserById(updatedUser.getId()));
    }

    @Test
    public void roleWorkFlowTest() {
        Role newRole = new Role();
        newRole.setName("NewRole");

        Role createdRole = roleService.createRole(newRole);
        Assert.assertNotNull(createdRole.getId());
        newRole.setId(createdRole.getId());
        Assert.assertEquals(newRole, createdRole);

        Role selectedRole = roleService.getRoleById(createdRole.getId());
        Assert.assertEquals(createdRole, selectedRole);

        createdRole.setName("UpdateRole");
        Role updatedRole = roleService.updateRole(createdRole);
        Assert.assertEquals(createdRole, updatedRole);

        roleService.deleteRole(updatedRole.getId());
        Assert.assertNull(roleService.getRoleById(createdRole.getId()));
    }

    @Test
    public void userWithRoles() {
        Role tester = new Role();
        Role developer = new Role();
        tester.setName("Tester");
        developer.setName("Developer");
        Role createdRoleTester = roleService.createRole(tester);
        Role createdRoleDeveloper = roleService.createRole(developer);

        User newEmployee = new User();
        newEmployee.setName("Edward");
        newEmployee.setPassword("1111");
        newEmployee.getRoles().add(createdRoleTester.getId());
        newEmployee.getRoles().add(createdRoleDeveloper.getId());

        User hiredEmployee = userService.createUser(newEmployee);
        Assert.assertEquals(newEmployee.getRoles(), hiredEmployee.getRoles());

        User selectedUser = userService.getUserById(hiredEmployee.getId());
        Assert.assertEquals(hiredEmployee, selectedUser);

        Role teamLead = new Role();
        teamLead.setName("TeamLead");
        Role createdRoleTeamLead = roleService.createRole(teamLead);

        hiredEmployee.setName("Ted");
        hiredEmployee.setPassword("123456SecurityTrainingHello7890");
        hiredEmployee.getRoles().add(createdRoleTeamLead.getId());
        User updatedUser = userService.updateUser(hiredEmployee);
        hiredEmployee.setPassword(null);
        Assert.assertEquals(hiredEmployee, updatedUser);

        userService.deleteUser(updatedUser.getId());
        updatedUser.getRoles().forEach(roleService::deleteRole);
    }
}
