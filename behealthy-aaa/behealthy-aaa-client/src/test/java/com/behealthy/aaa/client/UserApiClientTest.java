package com.behealthy.aaa.client;

import com.behealthy.aaa.api.entity.User;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.web.client.MockServerRestTemplateCustomizer;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;

import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

@RunWith(SpringRunner.class)
public class UserApiClientTest {

    private static final String BASE_URL = "http://localhost:8083";

    private final UserApiClient userApiClient;
    private final RestTemplateBuilder restTemplateBuilder;
    private final MockServerRestTemplateCustomizer customizer;

    private static User comparableUser;

    public UserApiClientTest() {
        this.customizer = new MockServerRestTemplateCustomizer();
        this.restTemplateBuilder = new RestTemplateBuilder(customizer);
        this.userApiClient = new UserApiClient(BASE_URL, restTemplateBuilder);
    }

    @BeforeClass
    public static void initComparableUser() {
        comparableUser = new User();
        comparableUser.setName("test");
    }

    @Test
    public void userApiTest() {
        customizer.getServer().expect(requestTo("http://localhost:8083/user"))
                .andRespond(withSuccess("{\"name\":\"test\"}", MediaType.APPLICATION_JSON));
        User user = userApiClient.createUser(new User());
        customizer.getServer().verify();
        Assert.assertEquals(comparableUser, user);
    }

    @Test
    public void getUserTest() {
        customizer.getServer().expect(requestTo("http://localhost:8083/user/73d5e874-e28c-4f96-b898-a6aa5adb1ce2"))
                .andRespond(withSuccess("{\"name\":\"test\"}", MediaType.APPLICATION_JSON));
        User createdUser = userApiClient.getUserById("73d5e874-e28c-4f96-b898-a6aa5adb1ce2");
        customizer.getServer().verify();
        Assert.assertEquals(comparableUser, createdUser);
    }

    @Test
    public void updateRoleTest() {
        customizer.getServer().expect(requestTo("http://localhost:8083/user/73d5e874-e28c-4f96-b898-a6aa5adb1ce2"))
                .andRespond(withSuccess("{\"name\":\"test\"}", MediaType.APPLICATION_JSON));
        User user = new User();
        user.setId("73d5e874-e28c-4f96-b898-a6aa5adb1ce2");
        User createdUser = userApiClient.updateUser(user);
        customizer.getServer().verify();
        Assert.assertEquals(comparableUser, createdUser);
    }

    @Test
    public void deleteRoleTest() {
        customizer.getServer().expect(requestTo("http://localhost:8083/user/73d5e874-e28c-4f96-b898-a6aa5adb1ce2"))
                .andRespond(withSuccess("true", MediaType.APPLICATION_JSON));
        userApiClient.deleteUser("73d5e874-e28c-4f96-b898-a6aa5adb1ce2");
        customizer.getServer().verify();
    }
}
