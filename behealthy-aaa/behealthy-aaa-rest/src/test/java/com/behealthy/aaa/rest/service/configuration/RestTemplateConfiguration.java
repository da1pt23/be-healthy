package com.behealthy.aaa.rest.service.configuration;

import com.behealthy.aaa.api.service.AuthService;
import com.behealthy.aaa.security.interceptor.RestTemplateTokenSetterInterceptor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.boot.web.client.RestTemplateCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

import java.util.List;

public class RestTemplateConfiguration {

    @Bean
    public RestTemplateCustomizer restTemplateCustomizer(@Value("${security.open.urls}") List<String> securityOpenUrls,
                                                         AuthService authService,
                                                         @Value("${jwt.token.expiration.timeout}") Long tokenExpirationTimeout,
                                                         @Value("${service.account.name}") String accountName,
                                                         @Value("${service.account.password}") String password) {
        RestTemplateTokenSetterInterceptor interceptor = new RestTemplateTokenSetterInterceptor(securityOpenUrls, tokenExpirationTimeout, authService, accountName, password);
        return new RestTemplateCustomizer() {
            @Override
            public void customize(RestTemplate restTemplate) {
                restTemplate.getInterceptors().add(interceptor);
            }
        };
    }

    @Bean
    public RestTemplateBuilder restTemplateBuilder(RestTemplateCustomizer restTemplateCustomizer) {
        return new RestTemplateBuilder(restTemplateCustomizer);
    }
}
