INSERT INTO users (id, username, password) VALUES ('c2bcfbe5-935b-47d6-be50-a67ba9ce9383', 'SUPER', '$2a$10$vHyD9YGNBRUw6YNrEMM1/uL4NKMAdnuIfogNnyQgJc/vY2O5scL/e');
INSERT INTO users (id, username, password) VALUES ('e4b5225f-8699-4c14-81d5-10974115397b', 'behealthy-web', '$2a$10$OawCdLtHZ1U0Olo9p3Xg8.R9v5pnLKS/l1Tf0RUrzTpnnhGd.Blj2');

INSERT INTO role (id, name)
VALUES ('73d5e874-e28c-4f96-b898-a6aa5adb1ce2', 'ADMIN');
INSERT INTO role (id, name)
VALUES ('74d5e999-L28c-4f96-b898-a6aa5adb1ce2', 'USER');
INSERT INTO role (id, name)
VALUES ('95drfqwe-r29f-4f96-b898-a6aa5adb1ce2', 'behealthy-web');

INSERT INTO user_roles (user_id, role_id) VALUES ('c2bcfbe5-935b-47d6-be50-a67ba9ce9383', '73d5e874-e28c-4f96-b898-a6aa5adb1ce2');
INSERT INTO user_roles (user_id, role_id) VALUES ('e4b5225f-8699-4c14-81d5-10974115397b', '95drfqwe-r29f-4f96-b898-a6aa5adb1ce2');


INSERT INTO role_permissions (role_id, permission) VALUES ('73d5e874-e28c-4f96-b898-a6aa5adb1ce2', 'USER_ADD');
INSERT INTO role_permissions (role_id, permission) VALUES ('73d5e874-e28c-4f96-b898-a6aa5adb1ce2', 'USER_GET');
INSERT INTO role_permissions (role_id, permission) VALUES ('73d5e874-e28c-4f96-b898-a6aa5adb1ce2', 'USER_DELETE');
INSERT INTO role_permissions (role_id, permission) VALUES ('73d5e874-e28c-4f96-b898-a6aa5adb1ce2', 'ROLE_ADD');
INSERT INTO role_permissions (role_id, permission) VALUES ('73d5e874-e28c-4f96-b898-a6aa5adb1ce2', 'ROLE_GET');
INSERT INTO role_permissions (role_id, permission) VALUES ('73d5e874-e28c-4f96-b898-a6aa5adb1ce2', 'ROLE_UPDATE');
INSERT INTO role_permissions (role_id, permission) VALUES ('73d5e874-e28c-4f96-b898-a6aa5adb1ce2', 'ROLE_DELETE');
INSERT INTO role_permissions (role_id, permission) VALUES ('73d5e874-e28c-4f96-b898-a6aa5adb1ce2', 'USER_UPDATE');
INSERT INTO role_permissions (role_id, permission) VALUES ('95drfqwe-r29f-4f96-b898-a6aa5adb1ce2', 'TOKEN_CHECK');
INSERT INTO role_permissions (role_id, permission) VALUES ('95drfqwe-r29f-4f96-b898-a6aa5adb1ce2', 'TOKEN_INVALIDATE');
INSERT INTO role_permissions (role_id, permission) VALUES ('95drfqwe-r29f-4f96-b898-a6aa5adb1ce2', 'USER_ADD');
INSERT INTO role_permissions (role_id, permission) VALUES ('95drfqwe-r29f-4f96-b898-a6aa5adb1ce2', 'USER_GET');
INSERT INTO role_permissions (role_id, permission) VALUES ('95drfqwe-r29f-4f96-b898-a6aa5adb1ce2', 'USER_DELETE');
INSERT INTO role_permissions (role_id, permission) VALUES ('95drfqwe-r29f-4f96-b898-a6aa5adb1ce2', 'ROLE_ADD');
INSERT INTO role_permissions (role_id, permission) VALUES ('95drfqwe-r29f-4f96-b898-a6aa5adb1ce2', 'ROLE_GET');
INSERT INTO role_permissions (role_id, permission) VALUES ('95drfqwe-r29f-4f96-b898-a6aa5adb1ce2', 'ROLE_UPDATE');
INSERT INTO role_permissions (role_id, permission) VALUES ('95drfqwe-r29f-4f96-b898-a6aa5adb1ce2', 'ROLE_DELETE');
INSERT INTO role_permissions (role_id, permission) VALUES ('95drfqwe-r29f-4f96-b898-a6aa5adb1ce2', 'USER_UPDATE');
INSERT INTO role_permissions (role_id, permission) VALUES ('95drfqwe-r29f-4f96-b898-a6aa5adb1ce2', 'TOKEN_CHECK');