create table address
(
    id     UUID PRIMARY KEY,
    profile_id UUID,
    city   VARCHAR(36),
    street VARCHAR(36),
    flat   VARCHAR(36),
    floor  VARCHAR(36),
    FOREIGN KEY (profile_id) REFERENCES profile (id) ON DELETE CASCADE
);

create table profile
(
    id         UUID PRIMARY KEY,
    username   VARCHAR(36) UNIQUE,
    birthday   DATE,
    email      VARCHAR(64) UNIQUE,
    phone      VARCHAR(64) UNIQUE
);