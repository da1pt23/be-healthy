package com.behealthy.web.service;

import com.behealthy.aaa.api.entity.User;
import com.behealthy.aaa.api.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RegistrationService {

    private final UserService userApiClient;
    private final String userDefaultRoleId;


    @Autowired
    public RegistrationService(UserService userService,
                               @Value("${new.user.default.role.id}") String userDefaultRoleId) {
        this.userApiClient = userService;
        this.userDefaultRoleId = userDefaultRoleId;
    }

    public User register(User user) {
        List<String> roles = user.getRoles();
        user.getRoles().clear();
        roles.add(userDefaultRoleId);
        return userApiClient.createUser(user);
    }
}
