package com.behealthy.aaa.api.entity;

public enum Permission {
    USER_ADD,
    USER_GET,
    USER_UPDATE,
    USER_DELETE,
    ROLE_ADD,
    ROLE_GET,
    ROLE_UPDATE,
    ROLE_DELETE,
    TOKEN_CHECK,
    TOKEN_INVALIDATE;
}
