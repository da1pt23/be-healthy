package com.behealthy.aaa.rest.service;

import com.behealthy.aaa.api.service.TokenService;
import com.behealthy.aaa.rest.repository.TokenRepository;
import org.springframework.stereotype.Service;

@Service
public class JdbcTokenService implements TokenService {

    private final TokenRepository tokenRepository;

    public JdbcTokenService(TokenRepository tokenRepository) {
        this.tokenRepository = tokenRepository;
    }

    @Override
    public Boolean isValidToken(String token) {
        return tokenRepository.isValidToken(token);
    }

    @Override
    public void invalidateToken(String token) {
        tokenRepository.invalidateToken(token);
    }

}
