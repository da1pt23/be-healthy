package com.behealthy.web.config;

import com.behealthy.aaa.api.service.AuthService;
import com.behealthy.aaa.api.service.TokenService;
import com.behealthy.aaa.api.service.UserService;
import com.behealthy.aaa.client.AuthApiClient;
import com.behealthy.aaa.client.UserApiClient;
import com.behealthy.aaa.security.interceptor.RestTemplateTokenSetterInterceptor;
import com.behealthy.aaa.security.jwt.TokenApiClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.boot.web.client.RestTemplateCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Configuration
public class SecurityConfiguration {

    @Bean
    public RestTemplateCustomizer restTemplateCustomizer(@Value("${security.open.urls}") List<String> securityOpenUrls,
                                                         AuthService authService,
                                                         @Value("${jwt.token.expiration.timeout}") Long tokenExpirationTimeout,
                                                         @Value("${service.account.name}") String accountName,
                                                         @Value("${service.account.password}") String password) {
        RestTemplateTokenSetterInterceptor interceptor = new RestTemplateTokenSetterInterceptor(securityOpenUrls, tokenExpirationTimeout, authService, accountName, password);
        return new RestTemplateCustomizer() {
            @Override
            public void customize(RestTemplate restTemplate) {
                restTemplate.getInterceptors().add(interceptor);
            }
        };
    }

    @Bean
    public TokenService tokenService(@Value("${aaa.rest.url}") String url, RestTemplateBuilder restTemplateBuilder) {
        return new TokenApiClient(url, restTemplateBuilder);
    }

    @Bean
    public AuthService authService(@Value("${aaa.rest.url}") String url) {
        return new AuthApiClient(url, new RestTemplateBuilder());
    }

    @Bean
    public UserService registerUserApiClient(@Value("${aaa.rest.url}") String url, RestTemplateBuilder restTemplateBuilder) {
        return new UserApiClient(url, restTemplateBuilder);
    }

}
