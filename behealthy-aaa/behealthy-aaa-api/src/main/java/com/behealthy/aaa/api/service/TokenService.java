package com.behealthy.aaa.api.service;

/**
 * Provide operations with a token
 */
public interface TokenService {

    /**
     * Checks if it's a valid token
     * @param token token
     * @return true if it's a valid token, else false
     */
    Boolean isValidToken(String token);

    /**
     * Invalidate token
     * @param token token
     */
    void invalidateToken(String token);
}
