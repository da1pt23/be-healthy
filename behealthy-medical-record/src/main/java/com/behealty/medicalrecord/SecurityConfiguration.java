package com.behealty.medicalrecord;

import com.behealthy.aaa.api.service.AuthService;
import com.behealthy.aaa.api.service.TokenService;
import com.behealthy.aaa.client.AuthApiClient;
import com.behealthy.aaa.security.interceptor.JwtInterceptor;
import com.behealthy.aaa.security.interceptor.RestTemplateTokenSetterInterceptor;
import com.behealthy.aaa.security.jwt.JwtTokenUtil;
import com.behealthy.aaa.security.jwt.PemKeyReader;
import com.behealthy.aaa.security.jwt.TokenApiClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.boot.web.client.RestTemplateCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.security.interfaces.RSAPublicKey;
import java.util.List;

@Configuration
public class SecurityConfiguration implements WebMvcConfigurer {

    @Autowired
    private JwtInterceptor jwtInterceptor;

    @Bean
    public RestTemplateCustomizer restTemplateCustomizer(@Value("${security.open.urls}") List<String> securityOpenUrls,
                                                         AuthService authService,
                                                         @Value("${jwt.token.expiration.timeout}") Long tokenExpirationTimeout,
                                                         @Value("${service.account.name}") String accountName,
                                                         @Value("${service.account.password}") String password) {
        RestTemplateTokenSetterInterceptor interceptor = new RestTemplateTokenSetterInterceptor(securityOpenUrls, tokenExpirationTimeout, authService, accountName, password);
        return new RestTemplateCustomizer() {
            @Override
            public void customize(RestTemplate restTemplate) {
                restTemplate.getInterceptors().add(interceptor);
            }
        };
    }

    @Bean
    public AuthService authService(@Value("${aaa.rest.url}") String url, RestTemplateBuilder restTemplateBuilder) {
        return new AuthApiClient(url, restTemplateBuilder);
    }

    @Bean
    public TokenService tokenService(@Value("${aaa.rest.url}") String url, RestTemplateBuilder restTemplateBuilder) {
        return new TokenApiClient(url, restTemplateBuilder);
    }

    @Bean
    public JwtTokenUtil jwtTokenUtil(@Value("${jwt.path.key.public}") String publicKeyFilePath,
                                     @Value("${jwt.algorithm.type}") String algorithmType,
                                     @Value("${jwt.algorithm.name}") String algorithmName,
                                     @Value("${jwt.issuer}") String issuer,
                                     @Value("${jwt.token.expiration.timeout}") Long tokenExpirationTimeout) throws Exception {
        RSAPublicKey publicKey = PemKeyReader.readPublicKeyFromFile(publicKeyFilePath, algorithmType);
        return new JwtTokenUtil(null, publicKey, algorithmName, issuer, tokenExpirationTimeout);
    }

    @Bean
    public JwtInterceptor jwtInterceptor(JwtTokenUtil jwtTokenUtil, TokenService tokenService) {
        return new JwtInterceptor(jwtTokenUtil, tokenService);
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(jwtInterceptor)
                .addPathPatterns("/**");
    }
}
