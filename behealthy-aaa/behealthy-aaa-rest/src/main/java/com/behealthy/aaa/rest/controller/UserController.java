package com.behealthy.aaa.rest.controller;

import com.behealthy.aaa.api.entity.RequiredPermissions;
import com.behealthy.aaa.api.entity.User;
import com.behealthy.aaa.api.service.UserService;
import com.behealthy.aaa.rest.service.JdbcUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;

import static com.behealthy.aaa.api.entity.Permission.*;

@RestController
@RequestMapping(value = "/user")
public class UserController {

    private final UserService service;

    @Autowired
    public UserController(JdbcUserService service) {
        this.service = service;
    }

    @RequiredPermissions(USER_ADD)
    @PostMapping
    public ResponseEntity<User> createUser(@RequestBody User user) {
        return new ResponseEntity<>(service.createUser(user), HttpStatus.OK);
    }

    @RequiredPermissions(USER_GET)
    @GetMapping(value = "/{userId}")
    public ResponseEntity<User> getUser(@PathVariable String userId) {
        User user = service.getUserById(userId);
        if (user == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(user, HttpStatus.OK);
        }
    }

    @RequiredPermissions(USER_UPDATE)
    @PutMapping(value = "/{userId}")
    public ResponseEntity<User> updateUser(@RequestBody User user, @PathVariable String userId) {
        if (!Objects.equals(user.getId(), userId)) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        User updatedUser = service.updateUser(user);
        if (updatedUser == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(updatedUser, HttpStatus.OK);
        }
    }

    @RequiredPermissions(USER_DELETE)
    @DeleteMapping(value = "/{userId}")
    public ResponseEntity<Void> deleteUser(@PathVariable String userId) {
        service.deleteUser(userId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
