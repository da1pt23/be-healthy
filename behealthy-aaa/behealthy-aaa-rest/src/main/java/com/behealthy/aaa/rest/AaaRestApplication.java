package com.behealthy.aaa.rest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AaaRestApplication {

    public static void main(String[] args) {
        SpringApplication.run(AaaRestApplication.class);
    }

}
