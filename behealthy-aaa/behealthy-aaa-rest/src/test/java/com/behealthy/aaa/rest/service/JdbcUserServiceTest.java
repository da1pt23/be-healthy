package com.behealthy.aaa.rest.service;

import com.behealthy.aaa.api.entity.User;
import com.behealthy.aaa.rest.repository.RoleRepository;
import com.behealthy.aaa.rest.repository.UserRepository;
import com.behealthy.aaa.rest.service.util.IdGenerator;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.crypto.password.PasswordEncoder;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class JdbcUserServiceTest {

    @InjectMocks
    private JdbcUserService userService;

    @Mock
    private UserRepository userRepository;
    @Mock
    private RoleRepository roleRepository;
    @Mock
    private PasswordEncoder passwordEncoder;
    @Mock
    private IdGenerator idGenerator;

    private User user;

    @Before
    public void initUser() {
        User user = new User();
        user.setId("1");
        user.setName("TestUser");
        user.setPassword("1111");
        this.user = user;
    }

    @Test
    public void updateUser() {
        //case with password
        when(userRepository.getUserById("1")).thenReturn(user);
        when(passwordEncoder.encode("1111")).thenReturn("encodedPassword");
        User updatedUser = userService.updateUser(user);
        Assert.assertEquals(updatedUser, user);
        Assert.assertNull(updatedUser.getPassword());
        verify(userRepository, times(1)).getUserById("1");
        verify(userRepository, times(1)).updateUserPassword(user);

        //case when password == null
        User userWithoutPassword = user;
        userWithoutPassword.setPassword(null);
        userService.updateUser(userWithoutPassword);
        verify(userRepository, times(2)).getUserById("1");
        verify(userRepository, times(1)).updateUserPassword(user);
    }

    @Test
    public void createUser() {
        when(passwordEncoder.encode("1111")).thenReturn("encodedPassword");
        when(userRepository.getUserById("1")).thenReturn(user);
        when(idGenerator.generateId()).thenReturn("1");
        User createdUser = userService.createUser(user);
        verify(userRepository, times(1)).getUserById("1");
        verify(passwordEncoder, times(1)).encode("1111");
        verify(idGenerator, times(1)).generateId();
        Assert.assertEquals(user, createdUser);
        Assert.assertEquals(user.getId(), createdUser.getId());
        Assert.assertNull(createdUser.getPassword());
    }

    @Test
    public void getUser() {
        when(userRepository.getUserById("1")).thenReturn(user);
        User foundUser = userService.getUserById("1");
        verify(userRepository, times(1)).getUserById("1");
        Assert.assertEquals(user, foundUser);
        Assert.assertEquals(user.getPassword(), foundUser.getPassword());
    }

}
