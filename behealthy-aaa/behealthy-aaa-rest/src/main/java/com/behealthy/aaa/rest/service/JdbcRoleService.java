package com.behealthy.aaa.rest.service;

import com.behealthy.aaa.api.entity.Role;
import com.behealthy.aaa.api.service.RoleService;
import com.behealthy.aaa.rest.repository.RoleRepository;
import com.behealthy.aaa.rest.service.util.IdGenerator;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
public class JdbcRoleService implements RoleService {

    private final RoleRepository roleRepository;
    private final IdGenerator idGenerator;

    public JdbcRoleService(RoleRepository roleRepository, IdGenerator idGenerator) {
        this.roleRepository = roleRepository;
        this.idGenerator = idGenerator;
    }

    @Override
    @Transactional
    public Role createRole(Role role) {
        role.setId(idGenerator.generateId());
        roleRepository.createRole(role);
        return roleRepository.getRoleById(role.getId());
    }


    @Override
    public Role getRoleById(String roleId) {
        return roleRepository.getRoleById(roleId);
    }

    @Override
    @Transactional
    public Role updateRole(Role role) {
        roleRepository.updateRole(role);
        return roleRepository.getRoleById(role.getId());
    }

    @Override
    @Transactional
    public void deleteRole(String id) {
        roleRepository.deleteRole(id);
    }
}
