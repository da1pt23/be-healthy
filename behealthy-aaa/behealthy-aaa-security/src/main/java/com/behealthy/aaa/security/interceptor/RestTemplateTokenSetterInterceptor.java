package com.behealthy.aaa.security.interceptor;


import com.behealthy.aaa.api.entity.User;
import com.behealthy.aaa.api.service.AuthService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;

public class RestTemplateTokenSetterInterceptor implements ClientHttpRequestInterceptor {

    private static final int MILISEC_TO_SECOND_DIVIDER = 1000;

    /**
     * Time in seconds
     */
    private final Long tokenExpirationTimeout;

    private final List<String> securityOpenUrls;
    private final AuthService authService;

    private final String accountName;
    private final String password;

    private String token;
    private LocalDateTime expiresAt;

    public RestTemplateTokenSetterInterceptor(List<String> securityOpenUrls,
                                              Long tokenExpirationTimeout,
                                              AuthService authService,
                                              String accountName,
                                              String password) {
        this.authService = authService;
        this.securityOpenUrls = securityOpenUrls;
        this.accountName = accountName;
        this.password = password;
        this.tokenExpirationTimeout = tokenExpirationTimeout / MILISEC_TO_SECOND_DIVIDER;
        this.expiresAt = LocalDateTime.now();
    }

    /**
     * If it's a request to the secure URL, we must provide a token.
     * If Bean has alive token we pass him, else we call {@code AuthService} for taking new token,
     * and write him in this instance of interceptor.
     * For both cases provide token as header Authorization.
     *
     * @param request http request
     * @param body    of request
     * @return http response
     * @throws IOException
     */
    @Override
    public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution) throws IOException {
        String url = request.getURI().toString();
        boolean isSecureUrl = securityOpenUrls.stream().noneMatch(x -> x.contains(url));

        if (isSecureUrl) {
            //Generates token only if we haven't a valid token
            if (this.expiresAt.isBefore(LocalDateTime.now()) || this.token == null ) {
                token = authService.auth(new User(accountName, password));
                expiresAt = LocalDateTime.now().plusSeconds(tokenExpirationTimeout);
            }
            request.getHeaders().add(HttpHeaders.AUTHORIZATION, token);
        }
        return execution.execute(request, body);
    }
}
