package com.behealty.medicalrecord.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MedicalRecordController {

    @GetMapping(path = "/patient")
    public String patient(){
        return "HELLO PATIENT";
    }

}
