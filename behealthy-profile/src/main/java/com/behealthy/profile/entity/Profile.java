package com.behealthy.profile.entity;

import java.util.Date;
import java.util.Objects;
import java.util.UUID;

public class Profile {
    private UUID id;
    private String username;
    private Date birthday;
    private String email;
    private String phone;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }


    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (other == null || getClass() != other.getClass()) {
            return false;
        }
        Profile profile = (Profile) other;
        return Objects.equals(id, profile.id) &&
                Objects.equals(username, profile.username) &&
                Objects.equals(birthday, profile.birthday) &&
                Objects.equals(email, profile.email) &&
                Objects.equals(phone, profile.phone);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, username, birthday, email, phone);
    }
}
