package com.behealthy.aaa.security.jwt;

import com.behealthy.aaa.api.service.TokenService;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.web.client.RestTemplate;

public class TokenApiClient implements TokenService {

    private static final String PATH_TOKEN_INVALIDATE = "/token/invalidate";
    public static final String PATH_CHECK = "/check";

    private final String baseUrl;
    private final RestTemplate restTemplate;

    public TokenApiClient(String baseUrl, RestTemplateBuilder restTemplateBuilder) {
        this.baseUrl = baseUrl;
        this.restTemplate = restTemplateBuilder.build();
    }

    @Override
    public Boolean isValidToken(String token) {
        String url = this.baseUrl + PATH_CHECK;
        HttpEntity<String> entity = new HttpEntity<>(token);
        return restTemplate.exchange(url, HttpMethod.POST, entity, Boolean.class).getBody();
    }

    @Override
    public void invalidateToken(String token) {
        HttpEntity<String> entity = new HttpEntity<>(token);
        restTemplate.exchange(baseUrl + PATH_TOKEN_INVALIDATE, HttpMethod.POST, entity, String.class);
    }
}
