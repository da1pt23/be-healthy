package com.behealthy.aaa.rest.service;

import com.behealthy.aaa.api.entity.Permission;
import com.behealthy.aaa.api.entity.User;
import com.behealthy.aaa.api.service.UserService;
import com.behealthy.aaa.rest.repository.RoleRepository;
import com.behealthy.aaa.rest.repository.UserRepository;
import com.behealthy.aaa.rest.service.util.IdGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional(readOnly = true)
public class JdbcUserService implements UserService {

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final PasswordEncoder passwordEncoder;
    private final IdGenerator idGenerator;

    @Autowired
    public JdbcUserService(UserRepository userRepository, RoleRepository roleRepository, PasswordEncoder passwordEncoder, IdGenerator idGenerator) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.passwordEncoder = passwordEncoder;
        this.idGenerator = idGenerator;
    }

    @Override
    @Transactional
    public User createUser(User user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setId(idGenerator.generateId());
        userRepository.createUser(user);
        roleRepository.saveUserRoles(user.getId(), user.getRoles());
        return getUserById(user.getId());
    }

    @Override
    public User getUserById(String userId) {
        User user = userRepository.getUserById(userId);
        if (user != null) {
            user.setPassword(null);
        }
        return user;
    }

    @Override
    @Transactional
    public User updateUser(User user) {
        userRepository.update(user);
        if (user.getPassword() != null) {
            user.setPassword(passwordEncoder.encode(user.getPassword()));
            userRepository.updateUserPassword(user);
        }
        roleRepository.deleteUserRoles(user.getId());
        roleRepository.saveUserRoles(user.getId(), user.getRoles());
        return getUserById(user.getId());
    }

    @Override
    @Transactional
    public void deleteUser(String id) {
        userRepository.deleteUser(id);
    }

    public User getUserByName(String userName) {
        return userRepository.getUserByName(userName);
    }

    public List<Permission> getUserPermissions(User user) {
        List<Permission> userPermissions = new ArrayList<>();
        user.getRoles().forEach(x -> userPermissions.addAll(roleRepository.getRolePermissions(x)));
        return userPermissions;
    }

}
